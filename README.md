# php-http-client-cookiebag
A psr-18 compliant middleware client that handles cookies for psr-7 http messages.


## Installation

The installation of this library is made via composer.
Download `composer.phar` from [their website](https://getcomposer.org/download/).
Then add to your composer.json :

```json
	"require": {
		...
		"php-extended/php-http-client-cookiebag": "^1",
		...
	}
```

Then run `php composer.phar update` to install this library.
The autoloading of all classes of this library is made through composer's autoloader.


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/* @var $client Psr\Http\Client\ClientInterface */    // psr-18
/* @var $request Psr\Http\Message\RequestInterface */ // psr-7

$client = new CookieBagClient($client);
$response = $client->sendRequest($request);

/* @var $response Psr\Http\Message\ResponseInterface */

```

This library handles the adding of `Cookie` header on the requests and the
decoding according to the `Set-Cookie` headers on the responses.


## License

MIT (See [license file](LICENSE)).
