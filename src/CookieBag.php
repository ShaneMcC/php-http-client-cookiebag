<?php

namespace PhpExtended\HttpClient;

use PhpExtended\SuffixList\PublicSuffixListBundle;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * CookieBag class file.
 * 
 * This class manages all the existing cookies, adds and removes them when
 * necessary, collects them from responses and adds them to appropriate
 * requests.
 * 
 * @see https://tools.ietf.org/html/rfc6265
 * 
 * @author Anastaszor
 */
class CookieBag
{
	
	/**
	 * All the cookies that are currently available and collected.
	 * 
	 * @var Cookie[]
	 */
	protected $_cookies = [];
	
	/**
	 * The configuration for this bag.
	 * 
	 * @var CookieBagConfiguration
	 */
	protected $_configuration = null;
	
	/**
	 * Builds a new CookieBag with the given settings.
	 * 
	 * @param CookieBagConfiguration $cookieBagConfiguration
	 */
	public function __construct(CookieBagConfiguration $configuration)
	{
		$this->_configuration = $configuration;
	}
	
	/**
	 * Adds the appropriate cookie to the specific request, and returns the
	 * newly created request with the cookies added.
	 * 
	 * @param RequestInterface $request
	 * @return RequestInterface
	 */
	public function addCookies(RequestInterface $request) : RequestInterface
	{
		if($this->_configuration->getReplaceRequestCookies())
			$request = $request->withoutHeader('Cookie');
		foreach($this->_cookies as $cookie)
		{
			if($cookie->matches($request))
			{
				$request = $request->withAddedHeader('Cookie', $cookie->getNameValuePairStr());
				$cookie->updateLastAccessTime();
			}
		}
		return $request;
	}
	
	/**
	 * Gathers the set-cookies from the given response and adds them to the
	 * persistant list of managed cookies.
	 * 
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 */
	public function collectCookies(ResponseInterface $response) : ResponseInterface
	{
		$targetUris = $response->getHeader('X-Target-Uri');
		if(empty($targetUris))
		{
			// as we cannot ensure that the website does not tries to set
			// cookies for other domains that itself, we deny the whole
			// possibility to set cookies
			return $response;
		}
		$targetUri = array_pop($targetUris);
		$hostname = parse_url($targetUri, PHP_URL_HOST);
		$hostpath = parse_url($targetUri, PHP_URL_PATH);
		if($hostname === false)
		{
			return $response;
		}
		
		$cookieFactory = new CookieFactory();
		foreach($response->getHeader('Set-Cookie') as $cookieHeader)
		{
			try
			{
				$cookie = $cookieFactory->parse($hostname, $hostpath, $cookieHeader);
			}
			catch(\RuntimeException $e)
			{
				// on failure, just ignore the cookie
				continue;
			}
			$this->handleNewCookie($hostname, $cookie);
		}
		if($this->_configuration->getRemoveResponseCookies())
			$response = $response->withoutHeader('Set-Cookie');
		
		// do garbage collection if needed
		$this->checkGc();
		
		return $response;
	}
	
	/**
	 * Handles the given cookie as to be added to the list of known cookies.
	 * This method replaces existing cookies by this one whenever needed, and
	 * ensure that this cookie is a legitimate cookie to keep.
	 * 
	 * @param string $hostname
	 * @param Cookie $cookie
	 * @return boolean whether the cookie was effectively added to the list
	 */
	protected function handleNewCookie(string $hostname, Cookie $cookie) : bool
	{
		$id = $cookie->getId();
		
		// if the cookie is expired, then reject it and cleanup just in case
		$timestamp = time();
		if($cookie->getExpires() !== null && $cookie->getExpires()->getTimestamp() < $timestamp)
		{
			unset($this->_cookies[$id]);
			return false;
		}
		if($cookie->getMaxAge() !== null && $cookie->getMaxAge()->getTimestamp() < $timestamp)
		{
			unset($this->_cookies[$id]);
			return false;
		}
		
		if(isset($this->_cookies[$id]))
		{
			// if the cookie already exists, then it is legitimate to replace
			// it when the server asks for it
			$cookie->recoverCreationTime($this->_cookies[$id]);
			$this->_cookies[$id] = $cookie;
			return true;
		}
		
		// if the domain of the request do not correspond to the domain of the
		// cookie, then we deny the cookie
		if(!$this->isDomainIncluded($cookie->getDomain(), $hostname))
			return false;
		
		// if the domain is in the suffix list, then the cookie is illegitimate
		// thus, we do not store it.
		$isTld = PublicSuffixListBundle::isInTheList($cookie->getDomain());
		if($isTld)
			return false;
		
		// finally, store the cookie
		$this->_cookies[$id] = $cookie;
		return true;
	}
	
	/**
	 * Gets whether the domainToInclude is included into the domainRecipient.
	 * 
	 * As per RFC6265, 'example.com' and 'foo.example.com' are included in
	 * 		'foo.example.com',
	 * but 'bar.example.com' and 'baz.foo.example.com' are not included in
	 * 		'foo.example.com'
	 * 
	 * @param string $domainToInclude
	 * @param string $domainRecipient
	 * @return boolean
	 */
	protected function isDomainIncluded(string $domainToInclude, string $domainRecipient) : bool
	{
		// TODO domain canonicalization with punycode
		// array map : remove trailing spaces
		// array filter : remove empty domain shards
		// array reverse : ensure the domain are from less generic (tld) to most generic
		// array values : ensure the index follows and matches
		$includes = array_values(array_reverse(array_filter(array_map('trim', explode('.', strtolower($domainToInclude))), 'strlen')));
		$recipients = array_values(array_reverse(array_filter(array_map('trim', explode('.', strtolower($domainRecipient))), 'strlen')));
		
		if(count($includes) > count($recipients))
			return false;
		
		for($i = 0; $i < count($includes); $i++)
		{
			if($includes[$i] !== $recipients[$i])
				return false;
		}
		
		return true;
	}
	
	/**
	 * Checks whether the garbage collection should be done. If so, does it.
	 */
	protected function checkGc()
	{
		$int = 1 + ((int) rand(0, 100));
		if($int > $this->_configuration->getGcProbability())
			$this->doGc();
	}
	
	/**
	 * Does the garbage collection. This will remove expired cookies, then
	 * cookies that share a domain field with more than allowed other cookies, 
	 * then older cookies until we get an acceptable number of cookies.
	 */
	protected function doGc()
	{
		$timestamp = time();
		foreach($this->_cookies as $id => $cookie)
		{
			if($cookie->getExpires() !== null && $cookie->getExpires()->getTimestamp() < $timestamp)
			{
				unset($this->_cookies[$id]);
			}
			if($cookie->getMaxAge() !== null && $cookie->getMaxAge()->getTimestamp() < $timestamp)
			{
				unset($this->_cookies[$id]);
			}
		}
		
		if(count($this->_cookies) > $this->_configuration->getQuantitySoftLimit())
		{
			// begin the hunt by domain
			$domains = [];
			foreach($this->_cookies as $cookie)
			{
				$domain = $cookie->getDomain();
				if(isset($domains[$domain]))
					$domains[$domain]++;
				else
					$domains[$domain] = 1;
			}
			foreach($domains as $domain => $quantity)
			{
				if($quantity > $this->_configuration->getQuantityLinkedLimit())
				{
					// remove older cookies until the limit is ok
					$cookies = $this->getCookiesOrderedByLastAccess($domain);
					$qtyToRemove = $quantity - $this->_configuration->getQuantityLinkedLimit();
					$i = 0;
					foreach($cookies as $cookieSet)
					{
						foreach($cookieSet as $cookie)
						{
							unset($this->_cookies[$cookie->getId()]);
							if($i >= $qtyToRemove)
								break;
							$i++;
						}
					}
				}
			}
		}
		
		if(count($this->_cookies) > $this->_configuration->getQuantitySoftLimit())
		{
			$qtyToRemove = count($this->_cookies) - $this->_configuration->getQuantitySoftLimit();
			// remove older cookies until the limit is ok
			$cookies = $this->getCookiesOrderedByLastAccess();
			$i = 0;
			foreach($cookies as $cookieSet)
			{
				foreach($cookieSet as $cookie)
				{
					unset($this->_cookies[$cookie->getId()]);
					if($i >= $qtyToRemove)
						break;
					$i++;
				}
			}
		}
		
		gc_collect_cycles();
	}
	
	/**
	 * Gets all the cookies belonging to given domain (or all cookies if no
	 * domain is given), ordered by date of last access.
	 * 
	 * @param string $domain
	 * @return Cookie[][]
	 */
	protected function getCookiesOrderedByLastAccess($domain = null) : array
	{
		$cookies = [];
		foreach($this->_cookies as $cookie)
		{
			$timestamp = $cookie->getLastAccessTime()->getTimestamp();
			if($domain === null || $this->isDomainIncluded($cookie->getDomain(), $domain))
			{
				if(isset($cookies[$timestamp]))
					$cookie[$timestamp][] = $cookie;
				else
					$cookies[$timestamp] = [$cookie];
			}
		}
		// ksort : sort by timestamp increasing, meaning older first
		ksort($cookies);
		return $cookies;
	}
	
}
