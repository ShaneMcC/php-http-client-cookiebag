<?php

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * CookieBagClient class file.
 * 
 * This class is an implementation of a client which does handle cookies
 * that are sent by the server and is able to send them back to the server
 * according to the target urls and other cookie parameters.
 * 
 * @author Anastaszor
 */
class CookieBagClient implements ClientInterface
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected $_client = null;
	
	/**
	 * The inner cookie bag.
	 * 
	 * @var CookieBag
	 */
	protected $_cookie_bag = null;
	
	/**
	 * Whether this client is enabled.
	 * 
	 * @var boolean
	 */
	protected $_enabled = true;
	
	/**
	 * Builds a new CookieBagClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param CookieBagConfiguration $configuration
	 * @param boolean $enabled
	 */
	public function __construct(ClientInterface $client, CookieBagConfiguration $configuration = null, bool $enabled = true)
	{
		$this->_client = $client;
		if($configuration === null)
			$configuration = new CookieBagConfiguration();
		$this->_cookie_bag = new CookieBag($configuration);
		$this->_enabled = $enabled;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request): ResponseInterface
	{
		if(!$this->_enabled)
			return $this->_client->sendRequest($request);
		
		$request = $this->_cookie_bag->addCookies($request);
		
		$response = $this->_client->sendRequest($request);
		
		$response = $this->_cookie_bag->collectCookies($response);
		
		return $response;
	}
	
}
