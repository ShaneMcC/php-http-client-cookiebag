<?php

namespace PhpExtended\HttpClient;

/**
 * CookieFactory class file.
 * 
 * This class parses the cookie strings and create cookies according to RFC6265.
 * 
 * @author Anastaszor
 */
class CookieFactory
{
	
	/**
	 * Parses the given string and creates a new cookie.
	 * 
	 * @param string $requestHostname
	 * @param string $requestHostpath
	 * @param string $cookieString
	 * @return Cookie
	 * @throws \RuntimeException if the cookie cannot be parsed.
	 */
	public function parse($requestHostname, $requestHostpath, $cookieString) : Cookie
	{
		$values = array_map('trim', explode(';', $cookieString));
		$first = true;
		$cookie = new Cookie();
		foreach($values as $keyPair)
		{
			$data = array_map('trim', explode('=', $keyPair, 2));
			if($first)
			{
				if(!isset($data[1]))
					throw new \RuntimeException(strtr('Failed to find value for cookie {name}',
						['{name}' => $data[0]]));
				if(empty($data[0]))
					throw new \RuntimeException(strtr('Failed to find name for cookie {value}',
						['{value}' => $data[1]]));
				$cookie->setName($data[0]);
				$cookie->setValue($data[1]);
				$first = false;
			}
			else
			{
				switch(strtolower($data[0]))
				{
					case 'expires':
						// just ignore expires without values
						if(!isset($data[1])) break;
						$dt = \DateTime::createFromFormat(\DateTime::COOKIE, $data[1]);
						// just ignore expires with wrong date format
						if($dt === false) break;
						$cookie->setExpires($dt);
						$cookie->setPersistent(true);
						break;
					
					case 'max-age':
						// just ignore max-age without values
						if(!isset($data[1]) || !is_numeric($data[1])) break;
						$dt = new \DateTime();
						$data[1] = (integer) $data[1];
						$interval = \DateInterval::createFromDateString('+ '.$data[1].' seconds');
						$dt->add($interval);
						$cookie->setMaxAge($dt);
						$cookie->setPersistent(true);
						break;
					
					case 'domain':
						// just ignore cookies with wrong domain values
						if(!isset($data[1]) || empty($data[1])) break;
						$cookie->setDomain($data[1]);
						$cookie->setHostOnly(false);
						break;
					
					case 'path':
						// just ignore cookies with wrong path values
						if(!isset($data[1]) | empty($data[1])) break;
						$cookie->setPath($data[1]);
						break;
					
					case 'secure':
						$cookie->setSecure(true);
						break;
					
					case 'httponly':
						$cookie->setHttpOnly(true);
						break;
					
					case 'extension':
						// just ignore cookies with wrong extension values
						if(!isset($data[1]) || empty($data[1])) break;
						$cookie->setExtension($data[1]);
						break;
						
					default:
						// just ignore other values
				}
			}
		}
		
		if(empty($cookie->getDomain()))
			$cookie->setDomain($requestHostname);
		if(empty($cookie->getPath()))
			$cookie->setPath($requestHostpath);
		
		return $cookie;
	}
	
}
