<?php

namespace PhpExtended\HttpClient;

/**
 * CookieBagConfiguration class file.
 * 
 * This class represents the configuration for the cookie bag.
 * 
 * @author Anastaszor
 */
class CookieBagConfiguration
{
	
	/**
	 * Whether to replace cookies that may already exists in the request, and
	 * only provides those that are available in this bag. Defaults to false,
	 * and keep previous cookies on the request.
	 * 
	 * @var string
	 */
	protected $_replace_request_cookies = false;
	
	/**
	 * Whether to remove cookies that are present in the response, and gets only
	 * pass the other headers for the response. Defaults to false, meaning the
	 * response will keep all of its cookie headers.
	 * 
	 * @var string
	 */
	protected $_remove_response_cookies = false;
	
	/**
	 * The quantity of cookies to hold before triggering any garbage collection.
	 * 
	 * @var integer
	 */
	protected $_quantity_soft_limit = 3000;
	
	/**
	 * The quantity of cookies that may shared a domain before triggering any
	 * garbage collection.
	 * 
	 * @var integer
	 */
	protected $_quantity_linked_limit = 50;
	
	/**
	 * The probability that garbage collection amongst cookies is triggered.
	 * Defaults to 10%.
	 * 
	 * @var integer
	 */
	protected $_gc_probability = 10;
	
	/**
	 * Enables the replacement of request cookies.
	 */
	public function enableReplaceRequestCookies()
	{
		$this->_replace_request_cookies = true;
	}
	
	/**
	 * Disables the replacement of request cookies.
	 */
	public function disableReplaceRequestCookies()
	{
		$this->_replace_request_cookies = false;
	}
	
	/**
	 * Gets whether to replace request cookies.
	 * 
	 * @return boolean
	 */
	public function getReplaceRequestCookies() : bool
	{
		return $this->_replace_request_cookies;
	}
	
	/**
	 * Enables the removal of response cookies.
	 */
	public function enableRemoveResponseCookies()
	{
		$this->_remove_response_cookies = true;
	}
	
	/**
	 * Disables the removal of response cookies.
	 */
	public function disableRemoveResponseCookies()
	{
		$this->_remove_response_cookies = false;
	}
	
	/**
	 * Gets whether to remove response cookies.
	 * 
	 * @return boolean
	 */
	public function getRemoveResponseCookies() : bool
	{
		return $this->_remove_response_cookies;
	}
	
	/**
	 * Sets the quantity soft limit.
	 * 
	 * @param integer $value
	 */
	public function setQuantitySoftLimit(int $value)
	{
		if($value <= 0) return;
		$this->_quantity_soft_limit = $value;
	}
	
	/**
	 * Gets the quantity soft limit.
	 * 
	 * @return integer
	 */
	public function getQuantitySoftLimit() : int
	{
		return $this->_quantity_soft_limit;
	}
	
	/**
	 * Sets the quantity linked limit.
	 * 
	 * @param integer $value
	 */
	public function setQuantityLinkedLimit(int $value)
	{
		if($value <= 0) return;
		$this->_quantity_linked_limit = $value;
	}
	
	/**
	 * Gets the quantity linked limit.
	 * 
	 * @return integer
	 */
	public function getQuantityLinkedLimit() : int
	{
		return $this->_quantity_linked_limit;
	}
	
	/**
	 * Sets the garbage collector probability (0-100, outside range values are
	 * cropped within the range).
	 * 
	 * @param integer $value
	 */
	public function setGcProbability(int $value)
	{
		if($value <= 0 || $value >= 100) return;
		$this->_gc_probability = $value;
	}
	
	/**
	 * Gets the garbage collector probability.
	 * 
	 * @return integer
	 */
	public function getGcProbability() : int
	{
		return $this->_gc_probability;
	}
	
}
