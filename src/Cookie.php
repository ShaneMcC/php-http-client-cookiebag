<?php

namespace PhpExtended\HttpClient;

use Psr\Http\Message\RequestInterface;

/**
 * Cookie class file.
 * 
 * This class represents a cookie according to RFC6265.
 * 
 * @author Anastaszor
 */
class Cookie
{
	
	/**
	 * The name of the cookie.
	 * 
	 * @var string
	 */
	protected $_name = null;
	
	/**
	 * The value of the cookie.
	 * 
	 * @var string
	 */
	protected $_value = null;
	
	/**
	 * When this cookie expires.
	 * 
	 * @var \DateTime
	 */
	protected $_expires = null;
	
	/**
	 * When this cookies expires².
	 * 
	 * @var \DateTime
	 */
	protected $_max_age = null;
	
	/**
	 * The domain to which this cookie is limited to.
	 * 
	 * @var string
	 */
	protected $_domain = null;
	
	/**
	 * The path to which this cookie is limited to.
	 * 
	 * @var string
	 */
	protected $_path = null;
	
	/**
	 * The creation time of the cookie.
	 * 
	 * @var \DateTime
	 */
	protected $_creation_time = null;
	
	/**
	 * The last access time of the cookie.
	 * 
	 * @var \DateTime
	 */
	protected $_last_access_time = null;
	
	/**
	 * Whether this cookie is persistent.
	 * 
	 * @var boolean
	 */
	protected $_persistent = false;
	
	/**
	 * Whether this cookie is host-only.
	 * 
	 * @var string
	 */
	protected $_host_only = true;
	
	/**
	 * Whether this cookie should only be sent to secure connections.
	 * 
	 * @var boolean
	 */
	protected $_secure = false;
	
	/**
	 * Whether this cookie should only be sent through http(s) connections.
	 * 
	 * @var boolean
	 */
	protected $_http_only = false;
	
	/**
	 * The extension of the cookie.
	 * 
	 * @var string
	 */
	protected $_extension = null;
	
	/**
	 * Builds a new Cookie with the given initialized variables.
	 */
	public function __construct()
	{
		$this->_creation_time = new \DateTime();
		$this->_last_access_time = new \DateTime();
	}
	
	/**
	 * Gets an unique identifier for this cookie.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->getDomain().'/'.trim($this->getPath(), '/').':'.$this->getName();
	}
	
	/**
	 * Gets the name of the cookie.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the name of the cookie.
	 * 
	 * @param string $_name
	 */
	public function setName(string $_name)
	{
		$this->_name = $_name;
	}
	
	/**
	 * Gets the value of the cookie.
	 * 
	 * @return string
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
	/**
	 * Sets the value of the cookie.
	 * 
	 * @param string $_value
	 */
	public function setValue(string $_value)
	{
		$this->_value = $_value;
	}
	
	/**
	 * Gets when this cookie expires.
	 * 
	 * @return \DateTime
	 */
	public function getExpires()
	{
		return $this->_expires;
	}
	
	/**
	 * Sets when this cookie expires.
	 * 
	 * @param \DateTime $_expires
	 */
	public function setExpires(\DateTime $_expires)
	{
		$this->_expires = $_expires;
	}
	
	/**
	 * Gets when this cookie expires.
	 * 
	 * @return \DateTime
	 */
	public function getMaxAge()
	{
		return $this->_max_age;
	}
	
	/**
	 * Sets when this cookie expires.
	 * 
	 * @param \DateTime $_max_age
	 */
	public function setMaxAge(\DateTime $_max_age)
	{
		$this->_max_age = $_max_age;
	}
	
	/**
	 * Gets the domain this cookie is limited to.
	 * 
	 * @return string
	 */
	public function getDomain()
	{
		return $this->_domain;
	}
	
	/**
	 * Sets the domain this cookie is limited to.
	 * 
	 * @param string $_domain
	 */
	public function setDomain(string $_domain)
	{
		$this->_domain = $_domain;
	}
	
	/**
	 * Gets the path this cookie is limited to.
	 * 
	 * @return string
	 */
	public function getPath()
	{
		return $this->_path;
	}
	
	/**
	 * Sets the path this cookie is limited to.
	 * 
	 * @param string $_path
	 */
	public function setPath(string $_path)
	{
		$this->_path = $_path;
		if(empty($this->_path))
			$this->_path = '/';
	}
	
	/**
	 * Gets the creation time for this cookie.
	 * 
	 * @return \DateTime
	 */
	public function getCreationTime() : \DateTime
	{
		return $this->_creation_time;
	}
	
	/**
	 * Updates the creation time of this cookie by the creation time of the
	 * other cookie.
	 * 
	 * @param Cookie $other
	 */
	public function recoverCreationTime(Cookie $other)
	{
		if($this->_creation_time->getTimestamp() > $other->_creation_time->getTimestamp())
			$this->_creation_time = $other->_creation_time;
	}
	
	/**
	 * Gets the last access time for this cookie.
	 * 
	 * @return \DateTime
	 */
	public function getLastAccessTime() : \DateTime
	{
		return $this->_last_access_time;
	}
	
	/**
	 * Updates the last access time to current time.
	 */
	public function updateLastAccessTime()
	{
		$this->_last_access_time = new \DateTime();
	}
	
	/**
	 * Gets whether this cookie is persistent.
	 * 
	 * @return boolean
	 */
	public function getPersistent() : bool
	{
		return $this->_persistent;
	}
	
	/**
	 * Sets the persistent flag.
	 * 
	 * @param boolean $_persistent
	 */
	public function setPersistent(bool $_persistent)
	{
		$this->_persistent = $_persistent;
	}
	
	/**
	 * Gets whether this cookie is host only.
	 * 
	 * @return string
	 */
	public function getHostOnly() : string
	{
		return $this->_host_only;
	}
	
	/**
	 * Sets the host only flag.
	 * 
	 * @param boolean $_host_only
	 */
	public function setHostOnly(bool $_host_only)
	{
		$this->_host_only = $_host_only;
	}
	
	/**
	 * Gets whether this cookie should only be sent to secure connections.
	 * 
	 * @return boolean
	 */
	public function getSecure() : bool
	{
		return $this->_secure;
	}
	
	/**
	 * Sets whether this cookie should only be sent to secure connections.
	 * 
	 * @param boolean $_secure
	 */
	public function setSecure(bool $_secure)
	{
		$this->_secure = $_secure;
	}
	
	/**
	 * Gets whether this cookie should only be sent by http(s) protocols.
	 * 
	 * @return boolean
	 */
	public function getHttpOnly() : bool
	{
		return $this->_http_only;
	}
	
	/**
	 * Sets whether this cookie should only be send by http(s) protocols.
	 * 
	 * @param boolean $_http_only
	 */
	public function setHttpOnly(bool $_http_only)
	{
		$this->_http_only = $_http_only;
	}
	
	/**
	 * Gets the extension of the cookie.
	 * 
	 * @return string
	 */
	public function getExtension() : string
	{
		return $this->_extension;
	}
	
	/**
	 * Sets the extension of the cookie.
	 * 
	 * @param string $_extension
	 */
	public function setExtension($_extension) : string
	{
		$this->_extension = $_extension;
	}
	
	/**
	 * Gets whether this cookie matches this given request. Returns true if it
	 * matches (i.e. it should be included with it), and false if it does not
	 * (i.e. it should not be included with it).
	 *  
	 * @param RequestInterface $request
	 * @return boolean
	 */
	public function matches(RequestInterface $request) : bool
	{
		if($request->getUri() === null) return false;
		return $this->expireMatches()
			&& $this->maxAgeMatches()
			&& $this->domainMatches($request->getUri()->getHost())
			&& $this->pathMatches($request->getUri()->getPath())
			&& $this->secureMatches($request->getUri()->getScheme())
			&& $this->httpOnlyMatches($request->getUri()->getScheme())
		;
	}
	
	/**
	 * Returns true if the expires data is still valid, else false.
	 * 
	 * @return boolean
	 */
	protected function expireMatches() : bool
	{
		return $this->_expires === null 
			|| $this->_expires->getTimestamp() > time();
	}
	
	/**
	 * Returns true if the max age data is still valid, else false.
	 * 
	 * @return boolean
	 */
	protected function maxAgeMatches() : bool
	{
		return $this->_max_age === null
			|| $this->_max_age->getTimestamp() > time();
	}
	
	/**
	 * Returns true if the domain of the cookie is included into the domain
	 * of the request, else false.
	 * 
	 * @param string $hostname
	 * @return boolean
	 */
	protected function domainMatches($hostname) : bool
	{
		if($this->_domain === null) 
			return true;
		
		// TODO domain canonicalization with punycode
		$includes = array_reverse(array_filter(array_map('trim', explode('.', strtolower($this->_domain))), 'strlen'));
		$recipients = array_reverse(array_filter(array_map('trim', explode('.', strtolower($hostname))), 'strlen'));
		
		if(count($includes) > count($recipients))
			return false;
		
		if($this->_host_only && count($recipients) > count($includes))
			return false;
		
		for($i = 0; $i < count($includes); $i++)
		{
			if($includes[$i] !== $recipients[$i])
				return false;
		}
		
		return true;
	}
	
	/**
	 * Returns true if the path of the cookie is included into the path of
	 * the request, else false.
	 * 
	 * @param string $path
	 * @return boolean
	 */
	protected function pathMatches($path) : bool
	{
		return $this->_path === null 
			|| 0 === strpos($path, $this->_path);
	}
	
	/**
	 * Returns true if the scheme of the request is allowed by the secure
	 * setting, else false.
	 * 
	 * @param string $scheme
	 * @return boolean
	 */
	protected function secureMatches($scheme) : bool
	{
		if(!$this->_secure)
			return true;
		return (boolean) preg_match('#^[a-z]+s#', strtolower($scheme));
	}
	
	/**
	 * Returns true if the scheme of the request is allowed by the http only
	 * setting, else false.
	 * 
	 * @param string $scheme
	 * @return boolean
	 */
	protected function httpOnlyMatches($scheme) : bool
	{
		if(!$this->_http_only)
			return true;
		return false !== strpos(strtolower($scheme), 'http');
	}
	
	/**
	 * GEts the string RFC-compliant version of the cookie for Cookie headers.
	 * 
	 * @return string
	 */
	public function getNameValuePairStr() : string
	{
		return $this->_name.'='.$this->_value;
	}
	
	/**
	 * Gets the string RFC-compliant version of the cookie for Set-Cookie headers.
	 * 
	 * @return string
	 */
	public function __toString() : string
	{
		$str = $this->getNameValuePairStr();
		if(!empty($this->_expires))
			$str .= '; Expires='.$this->_expires->format(\DateTime::COOKIE);
		if(!empty($this->_max_age))
			$str .= '; Max-Age='.($this->_max_age->getTimestamp() - time());
		if(!empty($this->_domain))
			$str .= '; Domain='.$this->_domain;
		if(!empty($this->_path))
			$str .= '; Path='.$this->_path;
		if(!empty($this->_secure))
			$str .= '; Secure';
		if(!empty($this->_http_only))
			$str .= '; HttpOnly';
		if(!empty($this->_extension))
			$str .= '; Extension='.$this->_extension;
		return $str;
	}
	
}
